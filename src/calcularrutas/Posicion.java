/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcularrutas;

/**
 *
 * @author ifatou
 */
public class Posicion {
    private double latitud  = 0;
    private double longitud = 0;

    public Posicion(double p_Latitud, double p_Longitud) {
        latitud = p_Latitud;
        longitud = p_Longitud;
    }

    public Posicion(String p_Latitud, String p_Longitud) {
        try{
            latitud  = Double.valueOf(p_Latitud);
            longitud = Double.valueOf(p_Longitud);
        } catch (NumberFormatException ex){
            System.out.println("Error al Convertir de Texto a Double");
        }
    }

    public boolean isNull(){
        if (latitud == 0 || longitud == 0){
            return true;
        }
        return false;
    }
    
    @Override
    public String toString(){
        if (!isNull()){
            return "(" + getLatitud() + "," + getLongitud() + ")";
        }
        return "";
    }

    /**
     * @return the latitud
     */
    public double getLatitud() {
        return latitud;
    }

    /**
     * @param latitud the latitud to set
     */
    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    /**
     * @return the longitud
     */
    public double getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
}
