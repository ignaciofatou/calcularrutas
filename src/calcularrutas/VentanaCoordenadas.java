/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcularrutas;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author ifatou
 */
public class VentanaCoordenadas extends javax.swing.JFrame {
    //Origen, Coordenadas de Inicio y Coordenadas Finales
    private static final Posicion POS_ORIGEN = new Posicion(36.67035, -5.449453);
    private ArrayList<Posicion> posicionesIni = new ArrayList();
    private ArrayList<Posicion> posicionesFin = new ArrayList();

    /**
     * Creates new form VentanaCoordenadas
     */
    public VentanaCoordenadas() {
        initComponents();
    }
    
    //Obtiene las Posiciones de Inicio Cargadas en la Ventana
    private ArrayList<Posicion> getPosicionesInicio(){
        ArrayList<Posicion> posicionesIni = new ArrayList();
        StringTokenizer st = new StringTokenizer(jTA_ListaOrigen.getText(), "\n");
        while (st.hasMoreTokens()){
            Posicion auxPosicion = castToPosicion(st.nextToken());
            if (auxPosicion != null){
                posicionesIni.add(auxPosicion);
            }
        }
        //Retornamos las Posiciones
        return posicionesIni;
    }
    
    //Convierte la Cadena de Coordenadas en un Objeto de tipo Posicion
    private Posicion castToPosicion(String p_Coordenada){
        if (!p_Coordenada.isEmpty()){
            String[] auxCadenas = p_Coordenada.split(",");
            if (auxCadenas.length == 2){
                Posicion posicion = new Posicion(auxCadenas[0].replace("(", "").trim(), auxCadenas[1].replace(")", "").trim());
                if (!posicion.isNull()){
                    return posicion;
                }
            }
        }
        return null;
    }

    private void calculaDistancias(){
        //Recuperamos todas las Posiciones de Inicio
        jTA_ListaDestino.setText("");
        posicionesIni = getPosicionesInicio();
        posicionesFin = getRutaOrdenada(posicionesIni);
        if ((posicionesFin != null) && (posicionesFin.size() > 0)){
            for (int x=0; x<posicionesFin.size(); x++){
                jTA_ListaDestino.append(posicionesFin.get(x).toString());
                if (x > 0){
                    //jTA_ListaDestino.append(" - Distancia: " + getDistanciaMetrosPuntos(posicionesFin.get(x-1), posicionesFin.get(x)));
                }
                jTA_ListaDestino.append("\n");
            }
        }
        jTA_ListaDestino.append("Fin");
        

    }
    
    private ArrayList<Posicion> getRutaOrdenada(ArrayList<Posicion> p_PosicionesInicio){
        if ((posicionesIni != null) && (posicionesIni.size() > 0)){
            ArrayList<Posicion> posicionesFinal = new ArrayList();
            posicionesFinal.add(POS_ORIGEN);
            while (p_PosicionesInicio.size() > 0){
                Posicion ultPosOrdenada = posicionesFinal.get(posicionesFinal.size() - 1);
                int posDistMin = -1;
                long distMin   = 99999999;
                for (int x=0; x < p_PosicionesInicio.size(); x++){
                    Posicion posActual = p_PosicionesInicio.get(x);
                    long auxDistancia = getDistanciaMetrosPuntos(ultPosOrdenada, posActual);
                    if (auxDistancia < distMin){
                        distMin = auxDistancia;
                        posDistMin = x;
                    }
                }
                if (posDistMin == -1){
                    System.out.println("Error");
                    break;
                }
                //Añadimos la Nueva Posicion
                posicionesFinal.add(p_PosicionesInicio.get(posDistMin));
                //Eliminamos la Posicion del ArrayList de Inicio
                p_PosicionesInicio.remove(posDistMin);
            }   
            return posicionesFinal;
        }
        return null;
    }
    
    private long getDistanciaMetrosPuntos(Posicion p_Posicion1, Posicion p_Posicion2){
        double distanciaM = getDistanciaKmPuntos(p_Posicion1.getLatitud(), p_Posicion1.getLongitud(), p_Posicion2.getLatitud(), p_Posicion2.getLongitud());
        return Math.round(distanciaM * 1000);
    }
    
    private static double getDistanciaKmPuntos(double lat1, double lng1, double lat2, double lng2) {  
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);  
        double dLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
   
        return distancia;  
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTA_ListaOrigen = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTA_ListaDestino = new javax.swing.JTextArea();
        jB_Ordenar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTA_ListaOrigen.setColumns(20);
        jTA_ListaOrigen.setRows(5);
        jTA_ListaOrigen.setBorder(javax.swing.BorderFactory.createTitledBorder("Introduzca Lista de Coordenadas"));
        jScrollPane1.setViewportView(jTA_ListaOrigen);

        jTA_ListaDestino.setColumns(20);
        jTA_ListaDestino.setRows(5);
        jTA_ListaDestino.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista de Coordenadas Ordenadas"));
        jScrollPane2.setViewportView(jTA_ListaDestino);

        jB_Ordenar.setText("Ordenar");
        jB_Ordenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_OrdenarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jB_Ordenar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jB_Ordenar)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jB_OrdenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_OrdenarActionPerformed
        calculaDistancias();
    }//GEN-LAST:event_jB_OrdenarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaCoordenadas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaCoordenadas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaCoordenadas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaCoordenadas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaCoordenadas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jB_Ordenar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTA_ListaDestino;
    private javax.swing.JTextArea jTA_ListaOrigen;
    // End of variables declaration//GEN-END:variables
}
